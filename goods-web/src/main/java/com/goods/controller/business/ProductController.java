package com.goods.controller.business;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 21:18
 * @version:
 */


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.service.ProductService;
import com.goods.common.annotation.ControllerEndpoint;
import com.goods.common.error.SystemException;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 物资资料管理
 **/
@Api(tags = "业务模块-物资资料管理接口")
@RestController
@RequestMapping("/business/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 物资资料列表
     *http://www.localhost:8989/business/product/findProductList?pageNum=1&pageSize=6&name=&categoryId=&supplier=&status=0&categorys=24,31,52
     * @param pageNum
     * @param pageSize
     * @param
     * @return
     */
    @ApiOperation(value = "物资资料列表", notes = "物资资料列表,根据部门名模糊查询")
    @GetMapping("/findProductList")
    public ResponseBean<PageVO<ProductVO>> findProductList(@RequestParam(value = "pageNum") Integer pageNum,
                                                           @RequestParam(value = "pageSize") Integer pageSize,
                                                           @RequestParam(value = "name") String name,
                                                           @RequestParam(value = "categoryId") String categoryId,
                                                           @RequestParam(value = "supplier") String supplier,
                                                           @RequestParam(value = "status") String status,
                                                           String categorys
                                                           ) {

        PageVO<ProductVO> pageVO = productService.findProductList(pageNum, pageSize, name,categoryId,supplier,status,categorys);
        return ResponseBean.success(pageVO);
    }

    /**
     * 根据条件查询资料列表
     * product/findProducts?pageNum=1&pageSize=6&status=0&categorys=33,34,37&name=aaa
     * @param map
     * @return
     */
    @ApiOperation(value = "根据条件查询资料列表")
    @GetMapping("/findProducts")
    public ResponseBean<PageVO<ProductVO>> findProducts(@RequestParam Map map){
        PageVO<ProductVO> productVOPageVO = productService.findProducts(map);
        return ResponseBean.success(productVOPageVO);
    }

    /**
     * 添加物资资料
     *
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "添加物资资料失败", operation = "添加物资资料")
    @RequiresPermissions({"product:add"})  //要求subject中必须含有product:add的权限才能执行方法add()。否则抛出异常AuthorizationException
    @ApiOperation(value = "添加物资来源")
    @PostMapping("/add")
    public ResponseBean add(@RequestBody @Validated ProductVO productVO) {
        productService.add(productVO);
        return ResponseBean.success();
    }

    /**
     * 编辑物资资料
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "编辑物资资料")
    @RequiresPermissions({"product:edit"})
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id) throws SystemException {
        ProductVO productVO = productService.edit(id);
        return ResponseBean.success(productVO);
    }

    /**
     * 更新物资资料
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "更新物资资料失败", operation = "更新物资资料")
    @ApiOperation(value = "更新物资资料")
    @RequiresPermissions({"product:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody @Validated ProductVO productVO) throws SystemException {
        productService.update(id, productVO);
        return ResponseBean.success();
    }

    /**
     * 将指定物资来源移至回收站
     *
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "将指定物资来源移至回收站失败", operation = "将指定物资来源移至回收站")
    @ApiOperation(value = "将指定物资来源移至回收站")
    @RequiresPermissions({"supplier:remove"})
    @PutMapping("/remove/{id}")
    public ResponseBean remove(@PathVariable Long id) throws SystemException {
        productService.remove(id);
        return ResponseBean.success();
    }

    /**
     * 加载库存信息
     * //product/findProductStocks?pageSize=9&pageNum=1&categorys=33,34,37
     * @param map
     * @return
     */
    @ApiOperation(value = "根据条件查询资料列表")
    @GetMapping("/findProductStocks")
    public ResponseBean findProductStocks(@RequestParam Map map){
        String pageNum = (String) map.get("pageNum");  //页码数
        String pageSize = (String) map.get("pageSize");//行数
        List<ProductStockVO> productVOPageVO = productService.findProductStocks(map);
        //分页
        PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(pageSize));
        PageInfo<ProductStockVO> pageInfo = new PageInfo<>(productVOPageVO);
        PageVO<ProductStockVO> productStockVOPageVO = new PageVO<>(pageInfo.getTotal(), productVOPageVO);
        return ResponseBean.success(productStockVOPageVO);
    }


    /**
     * 加载物资所有的库存信息
     * //business/product/findAllStocks?pageSize=9&pageNum=1&categorys=33,34,37
     * @param map
     * @return
     */
    @ApiOperation(value = "加载物资所有的库存信息")
    @GetMapping("/findAllStocks")
    public ResponseBean findAllStocks(@RequestParam Map map){
        List<ProductStockVO> productStockVOPageVO = productService.findProductStocks(map);
        return ResponseBean.success(productStockVOPageVO);
    }
}
