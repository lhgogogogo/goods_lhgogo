package com.goods.controller.business;

import com.goods.business.service.CategoryService;
import com.goods.common.annotation.ControllerEndpoint;
import com.goods.common.error.BusinessException;
import com.goods.common.error.SystemException;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ProductCategoryVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 10:21
 * @version:
 */

@Api(tags = "物资管理-物资类别接口")
@RestController
@RequestMapping("/business/productCategory")
public class ProductCategoryController {

    @Autowired
    private CategoryService businessService;


    /**
     * 加载父级分类数据
     * @return
     */
    @ApiOperation(value = "加载父级分类数据")
    @GetMapping("/getParentCategoryTree")
    public ResponseBean<List<ProductCategoryVO>> getParentCategoryTree(){
        List<ProductCategoryVO> productCategoryVOList = businessService.getParentCategoryTree();
        return ResponseBean.success(productCategoryVOList);
    }
    /**
     * 加载物资商品数据
     *
     * @return
     */
    @ApiOperation(value = "加载物资商品数据")
    @GetMapping("/categoryTree")
    public ResponseBean categoryTree(Integer pageNum, Integer pageSize) {
        PageVO<ProductCategoryVO> productCategorys =
                businessService.categoryTree(pageNum,pageSize);
        return ResponseBean.success(productCategorys);
    }


    /**
     * 编辑分类
     * @param id
     * @return
     */
    @ApiOperation(value = "编辑分类")
    @RequiresPermissions({"business:edit"})
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id) throws BusinessException{
        ProductCategoryVO departmentVO = businessService.edit(id);
        return ResponseBean.success(departmentVO);
    }

    /**
     * 更新分类
     * @param id
     * @return
     */
    @ApiOperation(value = "更新分类")
    @RequiresPermissions({"business:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody @Validated ProductCategoryVO productCategoryVO) throws SystemException, BusinessException {
        businessService.update(id,productCategoryVO);
        return ResponseBean.success();
    }

    /**
     * 删除分类
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "删除商品分类失败", operation = "删除分类")
    @ApiOperation(value = "删除分类")
    @RequiresPermissions({"business:delete"})
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id) throws BusinessException {
        businessService.delete(id);
        return ResponseBean.success();
    }

    /**
     * 添加分类
     *
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "添加分类失败", operation = "添加分类")
    @RequiresPermissions({"department:add"})
    @ApiOperation(value = "添加分类")
    @PostMapping("/add")
    public ResponseBean add(@RequestBody @Validated ProductCategoryVO productCategoryVO) {
        businessService.add(productCategoryVO);
        return ResponseBean.success();
    }


}
