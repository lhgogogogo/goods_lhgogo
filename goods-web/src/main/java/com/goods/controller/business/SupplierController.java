package com.goods.controller.business;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 19:04
 * @version:
 */


import com.goods.business.service.SupplierService;
import com.goods.common.annotation.ControllerEndpoint;
import com.goods.common.error.BusinessException;
import com.goods.common.error.SystemException;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 部门管理
 *
 **/
@Api(tags = "业务模块-物资来源接口")
@RestController
@RequestMapping("/business/supplier")
public class SupplierController {


    @Autowired
    private SupplierService supplierService;
    /**
     * 物资来源列表
     *
     * @return
     */
    @ApiOperation(value = "物资来源列表")
    @GetMapping("/findSupplierList")
    public ResponseBean<PageVO<SupplierVO>> findSupplierList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                                 @RequestParam(value = "pageSize") Integer pageSize,
                                                                 SupplierVO supplierVO) {
        PageVO<SupplierVO> list = supplierService.findSupplierList(pageNum, pageSize, supplierVO);
        return ResponseBean.success(list);
    }

    /**
     * 添加物资来源
     *
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "添加物资来源失败", operation = "添加物资来源")
    @RequiresPermissions({"supplier:add"})
    @ApiOperation(value = "添加物资来源")
    @PostMapping("/add")
    public ResponseBean add(@RequestBody @Validated SupplierVO supplierVO) {
        supplierService.add(supplierVO);
        return ResponseBean.success();
    }

    /**
     * 编辑物资来源
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "编辑物资来源")
    @RequiresPermissions({"supplier:edit"})
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id) throws BusinessException {
        SupplierVO supplierVO = supplierService.edit(id);
        return ResponseBean.success(supplierVO);
    }

    /**
     * 更新物资来源
     *
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "更新物资来源失败", operation = "更新物资来源")
    @ApiOperation(value = "更新物资来源")
    @RequiresPermissions({"supplier:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody @Validated SupplierVO supplierVO) throws SystemException {
        supplierService.update(id, supplierVO);
        return ResponseBean.success();
    }

    /**
     * 删除指定物资来源
     *
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "删除指定物资来源失败", operation = "删除指定物资来源")
    @ApiOperation(value = "删除指定物资来源")
    @RequiresPermissions({"supplier:delete"})
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id) throws SystemException {
        supplierService.delete(id);
        return ResponseBean.success();
    }

    /**
     * 查找所有物资来源
     *  /business/supplier/findAll
     * @param
     * @return
     */
    @ApiOperation(value = "查找所有物资来源")
    @GetMapping("/findAll")
    public ResponseBean findAll()  {
        List<SupplierVO> supplierVOList =  supplierService.findAll();
        return ResponseBean.success(supplierVOList);
    }
}
