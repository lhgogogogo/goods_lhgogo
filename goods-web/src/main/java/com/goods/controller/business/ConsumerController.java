package com.goods.controller.business;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 20:43
 * @version:
 */

import com.goods.business.service.ConsumerService;
import com.goods.common.annotation.ControllerEndpoint;
import com.goods.common.error.BusinessException;
import com.goods.common.error.SystemException;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 物资去处
 **/
@Api(tags = "业务模块-物资去处管理接口")
@RestController
@RequestMapping("/business/consumer")
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;

    /**
     * 物资去处分页并条件查询
     *  business/consumer/findConsumerList?pageNum=1&pageSize=10&name=aaa&address=%E5%AE%89%E5%BE%BD&contact=%E6%9E%97%E8%BE%89
     * @param map
     * @return
     */
    @ApiOperation(value = "入库记录列表")
    @GetMapping("/findConsumerList")
    public ResponseBean findConsumerList (@RequestParam Map map){
        PageVO<ConsumerVO> consumerVOPageVO = consumerService.findConsumerList(map);
        return ResponseBean.success(consumerVOPageVO);
    }


    /**
     * 查找所有物资去向
     *  //business/consumer/findAll
     * @param
     * @return
     */
    @ApiOperation(value = "查找所有物资去向")
    @GetMapping("/findAll")
    public ResponseBean findAll()  {
        List<ConsumerVO> consumerVOList =  consumerService.findAll();
        return ResponseBean.success(consumerVOList);
    }

    /**
     * 添加物资去处
     *
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "添加物资去处失败", operation = "添加物资去处")
    @RequiresPermissions({"consumer:add"})
    @ApiOperation(value = "添加物资来源")
    @PostMapping("/add")
    public ResponseBean add(@RequestBody @Validated ConsumerVO consumerVO) {
        consumerService.add(consumerVO);
        return ResponseBean.success();
    }

    /**
     * 编辑物资去处
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "编辑物资去处")
    @RequiresPermissions({"consumer:edit"})
    @GetMapping("/edit/{id}")
    public ResponseBean edit(@PathVariable Long id) throws BusinessException {
        ConsumerVO consumerVO = consumerService.edit(id);
        return ResponseBean.success(consumerVO);
    }

    /**
     * 更新物资来源
     *
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "更新物资去处失败", operation = "更新物资去处")
    @ApiOperation(value = "更新物资去处")
    @RequiresPermissions({"consumer:update"})
    @PutMapping("/update/{id}")
    public ResponseBean update(@PathVariable Long id, @RequestBody @Validated ConsumerVO consumerVO) throws SystemException, BusinessException {
        consumerService.update(id, consumerVO);
        return ResponseBean.success();
    }

    /**
     * 删除指定物资去处
     *
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "删除指定物资去处失败", operation = "删除指定物资去处")
    @ApiOperation(value = "删除指定物资去处")
    @RequiresPermissions({"consumer:delete"})
    @DeleteMapping("/delete/{id}")
    public ResponseBean delete(@PathVariable Long id) throws SystemException, BusinessException {
        consumerService.delete(id);
        return ResponseBean.success();
    }
}
