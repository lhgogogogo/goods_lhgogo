package com.goods.controller.business;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 1:50
 * @version:
 */

//http://www.localhost:8989/business/inStock/findInStockList?pageNum=1&pageSize=10&status=0&startTime=2021-11-24+00:00:00&endTime=2021-12-23+00:00:00

import com.goods.business.service.InStockService;
import com.goods.common.annotation.ControllerEndpoint;
import com.goods.common.error.BusinessException;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 入库记录管理
 **/
@Api(tags = "业务模块-入库记录管理接口")
@RestController
@RequestMapping("/business/inStock")
public class InStockController {

    @Autowired
    private InStockService inStockService;
    /**
     * 入库记录列表
     * @param map
     * @return
     */
    @ApiOperation(value = "入库记录列表")
    @GetMapping("/findInStockList")
    public ResponseBean findInStockList(@RequestParam Map map){
        PageVO<InStockVO> inStockVOPageVO = inStockService.findInStockList(map);
        return ResponseBean.success(inStockVOPageVO);
    }

    /**
     * 查看入库单明细
     * //business/inStock/detail/111?pageNum=1
     * @param id
     * @param pageNum
     * @return
     */
    @ApiOperation(value = "查看入库单明细")
    @GetMapping("detail/{id}")
    public ResponseBean detail(@PathVariable Integer id ,@RequestParam Integer pageNum) throws BusinessException {
        InStockDetailVO inStockDetailVO = inStockService.detail(id,pageNum);
        return ResponseBean.success(inStockDetailVO);
    }


    /**
     * 将指定入库记录移至回收站
     * //business/inStock/remove/112
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "将指定入库记录移至回收站失败", operation = "将指定入库记录移至回收站")
    @ApiOperation(value = "将指定入库记录移至回收站")
    @RequiresPermissions({"detail:remove"})
    @PutMapping("/remove/{id}")
    public ResponseBean remove(@PathVariable Integer id) throws BusinessException {
        inStockService.remove(id);
        return ResponseBean.success();
    }

    /**
     * 将指定入库记录还原
     * /business/inStock/back/111
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "将指定入库记录还原失败", operation = "将指定入库记录还原")
    @ApiOperation(value = "将指定入库记录还原")
    @RequiresPermissions({"detail:back"})
    @PutMapping("/back/{id}")
    public ResponseBean back(@PathVariable Integer id) throws BusinessException {
        inStockService.back(id);
        return ResponseBean.success();
    }



    /**
     * 新增入库
     * /business/inStock/addIntoStock
     * @return
     */
    @ApiOperation(value = "新增入库")
    @PostMapping("/addIntoStock")  //@Validated 帮助我们继续对传输的参数进行数据校验的注解
    public ResponseBean addIntoStock(@RequestBody @Validated InStockVO inStockVO, HttpServletRequest request){
        inStockService.addIntoStock(inStockVO,request);
        return ResponseBean.success();
    }

    /**
     * business/inStock/publish/113
     * 修改物资状态
     * @param id
     * @return
     */
    @ApiOperation(value = "修改物资状态")
    @PutMapping("publish/{id}")
    public ResponseBean publish(@PathVariable Integer id) throws BusinessException {
        inStockService.publish(id);
        return ResponseBean.success();
    }
}
