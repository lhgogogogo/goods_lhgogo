package com.goods.controller.business;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 23:11
 * @version:
 */

import com.goods.business.service.OutStockService;
import com.goods.common.annotation.ControllerEndpoint;
import com.goods.common.error.BusinessException;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 入库记录管理
 **/
@Api(tags = "业务模块-入库记录管理接口")
@RestController
@RequestMapping("/business/outStock")
public class OutStockController {
    @Autowired
    private OutStockService outStockService;

    /**
     * 出库记录列表
     * business/outStock/findOutStockList?pageNum=1&pageSize=10&status=0&outNum=11111111111&type=
     * @param map
     * @return
     */
    @ApiOperation(value = "出库记录列表")
    @GetMapping("/findOutStockList")
    public ResponseBean findOutStockList(@RequestParam Map map){
        PageVO<OutStockVO> outStockVOPageVO = outStockService.findOutStockList(map);
        return ResponseBean.success(outStockVOPageVO);
    }

    /**
     * 查看出库单明细
     * ///business/outStock/detail/15?pageNum=1
     * @param id
     * @param pageNum
     * @return
     */
    @ApiOperation(value = "查看入库单明细")
    @GetMapping("detail/{id}")
    public ResponseBean detail(@PathVariable Integer id , @RequestParam Integer pageNum) throws BusinessException {
        OutStockDetailVO outStockDetailVO = outStockService.detail(id,pageNum);
        return ResponseBean.success(outStockDetailVO);
    }

    /**
     * 将指定出库记录移至回收站
     * //business/inStock/remove/112
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "将指定出库记录移至回收站失败", operation = "将指定出库记录移至回收站")
    @ApiOperation(value = "将指定出库记录移至回收站")
    @RequiresPermissions({"detail:remove"})
    @PutMapping("/remove/{id}")
    public ResponseBean remove(@PathVariable Integer id) throws BusinessException {
        outStockService.remove(id);
        return ResponseBean.success();
    }

    /**
     * 将指定出库记录还原
     * /business/inStock/back/111
     * @param id
     * @return
     */
    @ControllerEndpoint(exceptionMessage = "将指定出库记录还原失败", operation = "将指定出库记录还原")
    @ApiOperation(value = "将指定出库记录还原")
    @RequiresPermissions({"detail:back"})
    @PutMapping("/back/{id}")
    public ResponseBean back(@PathVariable Integer id) throws BusinessException {
        outStockService.back(id);
        return ResponseBean.success();
    }


    /**
     * 新增出库
     * //8989/business/outStock/addOutStock
     * @return
     */
    @ApiOperation(value = "新增出库")
    @PostMapping("/addOutStock")  //@Validated 帮助我们继续对传输的参数进行数据校验的注解
    public ResponseBean addOutStock(@RequestBody @Validated OutStockVO outStockVO, HttpServletRequest request){
        outStockService.addOutStock(outStockVO,request);
        return ResponseBean.success();
    }

    /**
     * business/outStock/publish/113
     * 修改出库状态
     * @param id
     * @return
     */
    @ApiOperation(value = "修改出库状态")
    @PutMapping("publish/{id}")
    public ResponseBean publish(@PathVariable Integer id) throws BusinessException {
        outStockService.publish(id);
        return ResponseBean.success();
    }
}
