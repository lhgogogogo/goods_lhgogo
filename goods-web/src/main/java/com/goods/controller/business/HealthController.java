package com.goods.controller.business;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-10 1:23
 * @version:
 */

import com.goods.business.service.HealthService;
import com.goods.common.error.SystemException;
import com.goods.common.response.ResponseBean;
import com.goods.common.vo.business.HealthVO;
import com.goods.common.vo.system.PageVO;
import com.goods.common.vo.system.UserInfoVO;
import com.goods.system.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 健康打卡管理
 **/
@Api(tags = "业务模块-健康打卡管理接口")
@RestController
@RequestMapping("/business/health")
public class HealthController {
    @Autowired
    private HealthService healthService;

    @Autowired
    private UserService userService;

    /**
     * 今日是否已签到
     * @return
     */
    @ApiOperation(value = "是否健康打卡")
    @GetMapping("isReport")
    public ResponseBean isReport(){
        HealthVO healthVO = healthService.getHealth();
        if (null != healthVO.getId()){
        return ResponseBean.success(healthVO);
        }
        return ResponseBean.success();
    }

    /**
     * 打卡
     * /business/health/report
     * @return
     */
    @ApiOperation(value = "保存打卡记录")
    @PostMapping("report")
    public ResponseBean report(@RequestBody @Validated HealthVO healthVO) throws SystemException {
        UserInfoVO info = userService.info();
        String username = info.getUsername();
        healthService.report(healthVO,username);
        return ResponseBean.success();
    }

    /**
     * 获取签到列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    //business/health/history?pageSize=4&pageNum=1
    @GetMapping("history")
    public ResponseBean history(Integer pageNum, Integer pageSize){
        PageVO<HealthVO> healthVOPageVO = healthService.history(pageNum, pageSize);
        return ResponseBean.success(healthVOPageVO);
    }
}
