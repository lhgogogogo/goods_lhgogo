package com.goods.business.mapper;

import com.goods.common.model.business.OutStock;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 23:13
 * @version:
 */

public interface OutStockMapper extends Mapper<OutStock> {
}
