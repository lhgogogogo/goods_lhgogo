package com.goods.business.mapper;

import com.goods.common.model.business.OutStockInfo;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 23:51
 * @version:
 */

public interface OutStockInfoMapper extends Mapper<OutStockInfo> {
}
