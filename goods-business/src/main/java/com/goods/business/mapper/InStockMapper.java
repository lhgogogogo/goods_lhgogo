package com.goods.business.mapper;

import com.goods.common.model.business.InStock;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 2:02
 * @version:
 */

@MapperScan
public interface InStockMapper extends Mapper<InStock> {
}
