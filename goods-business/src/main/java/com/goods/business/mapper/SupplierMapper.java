package com.goods.business.mapper;

import com.goods.common.model.business.Supplier;
import com.goods.common.vo.business.SupplierVO;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 19:17
 * @version:
 */

@MapperScan
public interface SupplierMapper extends Mapper<Supplier> {
    List<Supplier> selectBySupplierVO(SupplierVO supplierVO);
}
