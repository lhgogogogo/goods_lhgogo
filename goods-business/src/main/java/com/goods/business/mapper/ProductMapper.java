package com.goods.business.mapper;

import com.goods.common.model.business.Product;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 22:09
 * @version:
 */


public interface ProductMapper extends Mapper<Product> {
}
