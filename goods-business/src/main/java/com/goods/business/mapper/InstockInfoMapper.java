package com.goods.business.mapper;

import com.goods.common.model.business.InStockInfo;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 11:29
 * @version:
 */

public interface InstockInfoMapper extends Mapper<InStockInfo> {
}
