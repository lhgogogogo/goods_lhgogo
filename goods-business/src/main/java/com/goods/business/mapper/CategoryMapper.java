package com.goods.business.mapper;

import com.goods.common.model.business.ProductCategory;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 10:52
 * @version:
 */
@MapperScan
public interface CategoryMapper extends Mapper<ProductCategory> {
}
