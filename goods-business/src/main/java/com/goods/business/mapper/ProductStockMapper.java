package com.goods.business.mapper;

import com.goods.common.model.business.ProductStock;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 11:53
 * @version:
 */

public interface ProductStockMapper extends Mapper<ProductStock> {
}
