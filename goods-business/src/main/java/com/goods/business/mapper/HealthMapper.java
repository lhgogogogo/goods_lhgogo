package com.goods.business.mapper;

import com.goods.common.model.business.Health;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-10 1:25
 * @version:
 */

public interface HealthMapper extends Mapper<Health> {
}
