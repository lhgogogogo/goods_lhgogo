package com.goods.business.mapper;

import com.goods.common.model.business.Consumer;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 20:51
 * @version:
 */

public interface ConsumerMapper extends Mapper<Consumer> {
    List<Consumer> selectConsumer(Map map);
}
