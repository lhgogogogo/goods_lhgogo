package com.goods.business.service;

import com.goods.common.error.BusinessException;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 23:12
 * @version:
 */

public interface OutStockService {
    PageVO<OutStockVO> findOutStockList(Map map);

    OutStockDetailVO detail(Integer id, Integer pageNum) throws BusinessException;

    void remove(Integer id) throws BusinessException;

    void back(Integer id) throws BusinessException;

    void addOutStock(OutStockVO outStockVO, HttpServletRequest request);

    void publish(Integer id) throws BusinessException;
}
