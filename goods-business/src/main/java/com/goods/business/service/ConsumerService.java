package com.goods.business.service;

import com.goods.common.error.BusinessException;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;
import java.util.Map;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 20:50
 * @version:
 */

public interface ConsumerService {
    PageVO<ConsumerVO> findConsumerList(Map map);

    void add(ConsumerVO consumerVO);

    ConsumerVO edit(Long id) throws BusinessException;

    void update(Long id, ConsumerVO consumerVO) throws BusinessException;

    void delete(Long id) throws BusinessException;

    List<ConsumerVO> findAll();
}
