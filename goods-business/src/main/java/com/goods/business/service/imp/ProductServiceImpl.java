package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.ProductMapper;
import com.goods.business.mapper.ProductStockMapper;
import com.goods.business.service.ProductService;
import com.goods.common.error.SystemCodeEnum;
import com.goods.common.error.SystemException;
import com.goods.common.model.business.Product;
import com.goods.common.model.business.ProductStock;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 21:34
 * @version:
 */

@Service
@SuppressWarnings("all")
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductStockMapper productStockMapper;

    /**
     * 物资资料列表
     * @param pageNum
     * @param pageSize
     * @param name
     * @param categoryId
     * @param supplier
     * @param status
     * @return
     */
    @Override
    public PageVO<ProductVO> findProductList(Integer pageNum, Integer pageSize, String name,
                                             String categoryId, String supplier, String status,String categorys) {
        PageHelper.startPage(pageNum,pageSize);
        //创建Example对象
        Example example = new Example(Product.class);
        Example.Criteria criteria = example.createCriteria();
        //依次进行判断
        if(!StringUtils.isEmpty(name)){
            criteria.andLike("name",name);
        }
        if(!StringUtils.isEmpty(categoryId)){
            criteria.andEqualTo("three_category_id",Long.parseLong(categoryId));
        }
        if(!StringUtils.isEmpty(status)){
            criteria.andEqualTo("status",Integer.parseInt(status));
        }
        //分类id
        //非空判断 防止空指针
        if(null != categorys){
            String[] split = categorys.split(",");
            int length = split.length;
            //防止数据越界
            if(length >= 1){
                criteria.andEqualTo("oneCategoryId", Integer.parseInt(split[0]));
                if(length>=2){
                    criteria.andEqualTo("twoCategoryId", Integer.parseInt(split[1]));
                    if(length >=3 ){
                        criteria.andEqualTo("threeCategoryId", Integer.parseInt(split[2]));

                    }
                }
            }
        }

        List<Product> products = productMapper.selectByExample(example);
        List<ProductVO> productVOS = new ArrayList<>();
        for (Product product : products) {
            ProductVO productVO = new ProductVO();
            BeanUtils.copyProperties(product,productVO);
            productVOS.add(productVO);
        }
        PageInfo<Product> productVOPageInfo = new PageInfo<>(products);
        return new PageVO<>(productVOPageInfo.getTotal(),productVOS);
    }

    /**
     * 添加物资
     * @param productVO
     */
    @Override
    public void add(ProductVO productVO) {
        Product product = new Product();
        BeanUtils.copyProperties(productVO,product);
        product.setCreateTime(new Date());
        product.setModifiedTime(new Date());
        product.setStatus(2);
        productMapper.insertSelective(product);
    }

    /**
     * 编辑物资资料
     * @param id
     * @return
     */
    @Override
    public ProductVO edit(Long id) throws SystemException {
        Product product = productMapper.selectByPrimaryKey(id);
        if (null == product){
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR,"要编辑的物资资料不存在");
        }
        ProductVO productVO = new ProductVO();
        BeanUtils.copyProperties(product,productVO);
        return productVO;
    }

    /**
     * 更新物资资料
     * @return
     */
    @Override
    public void update(Long id, ProductVO productVO) throws SystemException {
        Product product = productMapper.selectByPrimaryKey(id);
        if (null == product){
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR,"要更新的物资资料不存在");
        }
        Product product1 = new Product();
        BeanUtils.copyProperties(productVO,product1);
        product.setModifiedTime(new Date());
        productMapper.updateByPrimaryKeySelective(product1);
    }


    /**
     * 将指定物资来源移至回收站
     *
     * @param id
     * @return
     */
    @Override
    public void remove(Long id) throws SystemException {
        Product product = productMapper.selectByPrimaryKey(id);
        if (null == product){
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR,"要更新的物资资料不存在");
        }
        product.setModifiedTime(new Date());
        product.setStatus(1);
        productMapper.updateByPrimaryKeySelective(product);
    }
    /**
     * 根据条件查询资料列表
     * product/findProducts?pageNum=1&pageSize=6&status=0&categorys=33,34,37&name=aaa
     * @param map
     * @return
     */
    @Override
    public PageVO<ProductVO> findProducts(Map map) {
        String pageNum = (String) map.get("pageNum");  //页码数
        String pageSize = (String) map.get("pageSize");//行数
        String status = (String) map.get("status"); //状态
        String categorys = (String) map.get("categorys"); //分类id
        String name = (String) map.get("name"); //物资名称
        //创建example和criteria对象进行指定查询
        Example example = new Example(Product.class);
        Example.Criteria criteria = example.createCriteria();
        //匹配订单状态
        if (!StringUtils.isEmpty(status)) {
            criteria.andEqualTo("status", Integer.parseInt(status));
        }
        //物资类型
        if (!StringUtils.isEmpty(name)) {
            criteria.andEqualTo("name", Integer.parseInt(name));
        }
        //分类id
        //非空判断 防止空指针
        if(null != categorys){
            String[] split = categorys.split(",");
            int length = split.length;
            //防止数据越界
            if(length >= 1){
                criteria.andEqualTo("oneCategoryId", Integer.parseInt(split[0]));
                if(length>=2){
                    criteria.andEqualTo("twoCategoryId", Integer.parseInt(split[1]));
                    if(length >=3 ){
                        criteria.andEqualTo("threeCategoryId", Integer.parseInt(split[2]));

                    }
                }
            }
        }
        List<Product> products = productMapper.selectByExample(example);
        List<ProductVO> productVOList = products.stream().map(product -> {
            ProductVO productVO = new ProductVO();
            BeanUtils.copyProperties(product, productVO);
            return productVO;
        }).collect(Collectors.toList());
        PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(pageSize));
        PageInfo<Product> pageInfo = new PageInfo<>(products);
        return new PageVO<>(pageInfo.getTotal(), productVOList);
    }

    /**
     * 加载库存信息
     * //product/findProductStocks?pageSize=9&pageNum=1&categorys=33,34,37
     * @param map
     * @return
     */
    @Override
    public  List<ProductStockVO> findProductStocks(Map map) {
        String pageNum = (String) map.get("pageNum");  //页码数
        String pageSize = (String) map.get("pageSize");//行数
        String categorys = (String) map.get("categorys"); //分类id
        //查询所有ProductStock
        List<ProductStock> productStockList = productStockMapper.selectAll();
        //用于封装最后的集合
        List<ProductStockVO> productStockVOList = new ArrayList<>();
        //遍历集合 进行匹配
        for (ProductStock productStock : productStockList) {
            //分类id
            Example example = new Example(Product.class);
            Example.Criteria criteria = example.createCriteria();
            //非空判断 防止空指针
            if(null != categorys){
                String[] split = categorys.split(",");
                int length = split.length;
                //防止数据越界
                if(length > 0){
                    criteria.andEqualTo("oneCategoryId", Integer.parseInt(split[0]));
                    if(length > 1){
                        criteria.andEqualTo("twoCategoryId", Integer.parseInt(split[1]));
                        if(length > 2 ){
                            criteria.andEqualTo("threeCategoryId", Integer.parseInt(split[2]));
                        }
                    }
                }
            }

            criteria.andEqualTo("pNum",productStock.getPNum());
            Product product = productMapper.selectOneByExample(example);
            ProductStockVO productStockVO = new ProductStockVO();
            if(product != null){
                BeanUtils.copyProperties(productStock,productStockVO);
                BeanUtils.copyProperties(product,productStockVO);
                productStockVOList.add(productStockVO);
            }
        }
        return productStockVOList;
//        //分页
//        PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(pageSize));
//        PageInfo<ProductStockVO> pageInfo = new PageInfo<>(productStockVOList);
//        return new PageVO<>(pageInfo.getTotal(), productStockVOList);
    }
    /**
     * 加载物资所有的库存信息
     * //business/product/findAllStocks?pageSize=9&pageNum=1&categorys=33,34,37
     * @param map
     * @return
     */
   /* @Override
    public List<ProductStockVO> findAllStocks(Map map) {
        String pageNum = (String) map.get("pageNum");  //页码数
        String pageSize = (String) map.get("pageSize");//行数
        String categorys = (String) map.get("categorys"); //分类id
        //查询所有ProductStock
        List<ProductStock> productStockList = productStockMapper.selectAll();
        //用于封装最后的集合
        List<ProductStockVO> productStockVOList = new ArrayList<>();
        for (ProductStock productStock : productStockList) {
            ProductStockVO productStockVO = new ProductStockVO();
            BeanUtils.copyProperties(productStock,productStockVO);
            productStockVOList.add(productStockVO);
        }

        return productStockVOList;
    }*/
}
