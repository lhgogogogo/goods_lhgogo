package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.converter.CategoryConverter;
import com.goods.business.mapper.CategoryMapper;
import com.goods.business.service.CategoryService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.model.business.ProductCategory;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import com.goods.common.vo.business.ProductCategoryVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 10:38
 * @version:
 */

@Service
public class CategoryServiceImpl  implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 加载父级分类数据
     * @return
     */
    @Override
    public List getParentCategoryTree() {
        List<ProductCategory> productCategories = categoryMapper.selectAll();
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList =
                CategoryConverter.converterToProductCategoryVO(productCategories);
        return productCategoryTreeNodeVOList;
    }

    /**
     * 加载物资商品分类数据
     * @return
     */
    @Override
    public PageVO<ProductCategoryVO> categoryTree(Integer pageNum, Integer pageSize) {

        List<ProductCategory> productCategories = categoryMapper.selectAll();
        //类型转换
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOList =
                CategoryConverter.converterToProductCategoryTreeNodeVO(productCategories);
        if(null != pageNum && null != pageSize){
        PageHelper.startPage(pageNum,pageSize);
        }

        PageInfo pageInfo = new PageInfo<>(productCategories);
        return new PageVO(pageInfo.getTotal(),productCategoryTreeNodeVOList);
    }


    /**
     * 编辑分类
     * @param id
     * @return
     */
    @Override
    public ProductCategoryVO edit(Long id) throws BusinessException {
        ProductCategory productCategory = categoryMapper.selectByPrimaryKey(id);
        if(null == productCategory){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"该编辑的菜单不存在");
        }
        ProductCategoryVO productCategoryVO = new ProductCategoryVO();
        BeanUtils.copyProperties(productCategory,productCategoryVO);
        return productCategoryVO;
    }

    /**
     * 更新分类
     * @param id
     * @param productCategoryVO
     * @return
     */
    @Override
    public void update(Long id, ProductCategoryVO productCategoryVO) throws BusinessException {
        //查询ProductCategory对象
        ProductCategory productCategory = categoryMapper.selectByPrimaryKey(id);
        if(null == productCategory){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"要更新的菜单不存在");
        }
        ProductCategory productCategory1 = new ProductCategory();
        //转换
        BeanUtils.copyProperties(productCategoryVO,productCategory1);
        productCategory1.setId(id);
        productCategory1.setModifiedTime(new Date());
        categoryMapper.updateByPrimaryKeySelective(productCategory1);
    }

    /**
     * 删除分类
     * @param id
     * @return
     */
    @Override
    public void delete(Long id) throws BusinessException {
        ProductCategory productCategory = categoryMapper.selectByPrimaryKey(id);
        if(null == productCategory){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"要删除的类别不存在");
        }
        categoryMapper.deleteByPrimaryKey(id);
    }


    /**
     * 添加分类
     *
     * @return
     */
    @Override
    public void add(ProductCategoryVO productCategoryVO) {
        ProductCategory productCategory = new ProductCategory();
        BeanUtils.copyProperties(productCategoryVO,productCategory);
        productCategory.setCreateTime(new Date());
        productCategory.setModifiedTime(new Date());
        categoryMapper.insert(productCategory);
    }
}
