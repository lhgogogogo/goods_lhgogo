package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.SupplierMapper;
import com.goods.business.service.SupplierService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.error.SystemCodeEnum;
import com.goods.common.error.SystemException;
import com.goods.common.model.business.Supplier;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 19:15
 * @version:
 */

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierMapper supplierMapper;
    @Override
    public PageVO<SupplierVO> findSupplierList(Integer pageNum, Integer pageSize, SupplierVO supplierVO) {
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Supplier.class);
        List<Supplier> suppliers = supplierMapper.selectBySupplierVO(supplierVO);
        List<SupplierVO> supplierVOS = new ArrayList<>();
        suppliers.forEach(supplier -> {
            SupplierVO supplierVO1 = new SupplierVO();
            BeanUtils.copyProperties(supplier,supplierVO1);
            supplierVOS.add(supplierVO1);
        });
        PageInfo<SupplierVO> pageInfo = new PageInfo<>(supplierVOS);

        return new PageVO<>(pageInfo.getTotal(),supplierVOS);
    }

    /**
     * 添加物资来源
     *
     * @return
     */
    @Override
    public void add(SupplierVO supplierVO) {
        Supplier supplier = new Supplier();
        BeanUtils.copyProperties(supplierVO,supplier);
        supplier.setCreateTime(new Date());
        supplier.setModifiedTime(new Date());
        supplierMapper.insertSelective(supplier);
    }

    /**
     * 编辑物资来源
     *
     * @param id
     * @return
     */
    @Override
    public SupplierVO edit(Long id) throws BusinessException {
        Supplier supplier = supplierMapper.selectByPrimaryKey(id);
        if(null == supplier){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"该编辑的物资来源未找到");
        }
        SupplierVO supplierVO = new SupplierVO();
        BeanUtils.copyProperties(supplier,supplierVO);
        return supplierVO;
    }

    /**
     * 更新物资来源
     * @param id
     * @param supplierVO
     */

    @Override
    public void update(Long id, SupplierVO supplierVO) throws SystemException {
        Supplier supplier = supplierMapper.selectByPrimaryKey(id);
        if(supplier==null){
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR,"要更新的物资来源不存在");
        }
        Supplier supplier1 = new Supplier();
        BeanUtils.copyProperties(supplierVO,supplier1);
        supplier1.setId(id);
        supplier1.setModifiedTime(new Date());
        supplierMapper.updateByPrimaryKeySelective(supplier1);
    }

    /**
     * 删除物资来源
     * @param id
     */
    @Override
    public void delete(Long id) throws SystemException {
        Supplier supplier = supplierMapper.selectByPrimaryKey(id);
        if(supplier==null){
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR,"要删除的部门不存在");
        }
        supplierMapper.deleteByPrimaryKey(id);
    }

    /**
     * 查找所有物资来源
     *  /business/supplier/findAll
     * @param
     * @return
     */
    @Override
    public List<SupplierVO> findAll() {
        List<Supplier> suppliers = supplierMapper.selectAll();
        List<SupplierVO> supplierVOList = suppliers.stream().map(supplier -> {
            SupplierVO supplierVO = new SupplierVO();
            BeanUtils.copyProperties(supplier, supplierVO);
            return supplierVO;
        }).collect(Collectors.toList());
        return supplierVOList;
    }
}
