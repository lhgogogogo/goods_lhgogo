package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.HealthMapper;
import com.goods.business.service.HealthService;
import com.goods.common.model.business.Health;
import com.goods.common.model.system.User;
import com.goods.common.vo.business.HealthVO;
import com.goods.common.vo.system.PageVO;
import com.goods.system.mapper.UserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-10 1:25
 * @version:
 */

@Service
public class HealthServiceImpl implements HealthService {

    @Autowired
    private HealthMapper healthMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 获取今日打卡记录
     * @return
     */
    @Override
    public HealthVO getHealth() {
        long current=System.currentTimeMillis();    //当前时间毫秒数
        long zeroT=current/(1000*3600*24)*(1000*3600*24)- TimeZone.getDefault().getRawOffset();  //今天零点零分零秒的毫秒数
        Date dateZero = new Date(zeroT);
        String zero = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(zeroT);
//        long endT=zeroT+24*60*60*1000-1;  //今天23点59分59秒的毫秒数
//        String end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endT);

        Example example = new Example(Health.class);
        Example.Criteria criteria = example.createCriteria();

        criteria.andGreaterThan("createTime",dateZero);
        List<Health> healths = healthMapper.selectByExample(example);
        HealthVO healthVO = new HealthVO();
        if (healths.size() > 0){
            Health health = healths.get(healths.size() - 1);
            BeanUtils.copyProperties(health,healthVO);
        }
        return healthVO;
    }
    /**
     * 保存打卡记录
     *
     * @return
     */
    @Override
    public void report(HealthVO healthVO, String username) {
        Health health = new Health();
        User user = new User();
        user.setUsername(username);
        user = userMapper.selectOne(user);
        BeanUtils.copyProperties(healthVO,health);
        health.setUserId(user.getId());
        health.setCreateTime(new Date());
        healthMapper.insert(health);
    }

    /**
     * 获取签到列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageVO<HealthVO> history(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Health> healths = healthMapper.selectAll();
        List<HealthVO> healthVOList = healths.stream().map(health -> {
            HealthVO healthVO = new HealthVO();
            BeanUtils.copyProperties(health, healthVO);
            return healthVO;
        }).collect(Collectors.toList());
        PageInfo<Health> pageInfo = new PageInfo(healths);
        return new PageVO<>(pageInfo.getTotal(),healthVOList);
    }
}
