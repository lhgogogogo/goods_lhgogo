package com.goods.business.service;

import com.goods.common.error.BusinessException;
import com.goods.common.vo.business.ProductCategoryVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 10:37
 * @version:
 */

public interface CategoryService {


    List<ProductCategoryVO> getParentCategoryTree();


    PageVO<ProductCategoryVO> categoryTree(Integer pageNum, Integer pageSize);


    ProductCategoryVO edit(Long id) throws BusinessException;

    void update(Long id, ProductCategoryVO productCategoryVO) throws BusinessException;

    void delete(Long id) throws BusinessException;

    void add(ProductCategoryVO productCategoryVO);
}
