package com.goods.business.service.imp;

import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.*;
import com.goods.business.service.OutStockService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.model.business.*;
import com.goods.common.utils.JWTUtils;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.business.OutStockDetailVO;
import com.goods.common.vo.business.OutStockItemVO;
import com.goods.common.vo.business.OutStockVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 23:12
 * @version:
 */

@Service
@SuppressWarnings("all")
public class OutStockServiceImpl implements OutStockService {

    @Autowired
    private OutStockMapper outStockMapper;
    @Autowired
    private ConsumerMapper consumerMapper;
    @Autowired
    private OutStockInfoMapper outStockInfoMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductStockMapper productStockMapper;

    /**
     * 出库记录列表
     * business/outStock/findOutStockList?pageNum=1&pageSize=10&status=0&outNum=11111111111&type=
     * @param map
     * @return
     */
    @Override
    public PageVO<OutStockVO> findOutStockList(Map map) {
        String pageNum = (String) map.get("pageNum");  //页码数
        String pageSize = (String) map.get("pageSize");//行数
        String status = (String) map.get("status"); //状态
        String outNum = (String) map.get("outNum"); //状态
        String type = (String) map.get("type"); //状态
        //创建example和criteria对象进行指定查询
        Example example = new Example(OutStock.class);
        Example.Criteria criteria = example.createCriteria();
        //匹配订单状态
        if (!StringUtils.isEmpty(status)) {
            criteria.andEqualTo("status", Integer.parseInt(status));
        }
        //物资类型
        if (!StringUtils.isEmpty(type)) {
            criteria.andEqualTo("type", Integer.parseInt(type));
        }
        //物资订单号
        if (!StringUtils.isEmpty(outNum)) {
            criteria.andEqualTo("outNum", outNum);
        }
        //根据条件查询出OutStock集合
        List<OutStock> outStocks = outStockMapper.selectByExample(example);
        List<OutStockVO> outStockVOList = outStocks.stream().map(outStock -> {
            OutStockVO outStockVO = new OutStockVO();
            BeanUtils.copyProperties(outStock, outStockVO);
            Consumer consumer = consumerMapper.selectByPrimaryKey(outStock.getConsumerId());
            outStockVO.setPhone(consumer.getPhone());
            outStockVO.setName(consumer.getName());
            outStockVO.setOperator("admin");
            return outStockVO;
        }).collect(Collectors.toList());
        PageInfo<OutStock> pageInfo = new PageInfo<>(outStocks);
        return new PageVO<>(pageInfo.getTotal(), outStockVOList);
    }

    /**
     * 查看出库单明细
     * ///business/outStock/detail/15?pageNum=1
     * @param id
     * @param pageNum
     * @return
     */
    @Override
    public OutStockDetailVO detail(Integer id, Integer pageNum) throws BusinessException {
        //根据id查询InStock对象
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        //如果查询为空
        if (null == outStock) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND, "出库物资未找到");
        }
        //创建OutStockDetailVO 用于封装展示给前台的数据
        OutStockDetailVO outStockDetailVO = new OutStockDetailVO();
        BeanUtils.copyProperties(outStock,outStockDetailVO);
        //查询Consumer对象并将这个对象转换为ConsumerVO
        Consumer consumer = consumerMapper.selectByPrimaryKey(outStock.getConsumerId());
        ConsumerVO consumerVO = new ConsumerVO();
        BeanUtils.copyProperties(consumer,consumerVO);
        outStockDetailVO.setConsumerVO(consumerVO);

        //根据out_num查询StockInfo集合
        Example example = new Example(OutStockInfo.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("outNum",outStock.getOutNum());
        List<OutStockInfo> outStockInfos = outStockInfoMapper.selectByExample(example);
        //用于封装OutStockDetailVO中的List<OutStockItemVO>
        List<OutStockItemVO> outStockItemVOS = new ArrayList<>();
        int sum = 0; //用于保存总量
        //根据StockInfo中的p_num查询product
        //循环遍历
        for (OutStockInfo outStockInfo : outStockInfos) {
            sum += outStockInfo.getProductNumber();
            Product product = new Product();
            OutStockItemVO outStockItemVO = new OutStockItemVO();
            product.setPNum(outStockInfo.getPNum());
            product = productMapper.selectOne(product);
            BeanUtils.copyProperties(product,outStockItemVO);
            outStockItemVO.setCount(outStockInfo.getProductNumber());
            outStockItemVOS.add(outStockItemVO);
        }
        outStockDetailVO.setItemVOS(outStockItemVOS);
        return outStockDetailVO;
    }

    /**
     * 将指定出库记录移至回收站
     * //business/inStock/remove/112
     *
     * @param id
     * @return
     */
    @Override
    public void remove(Integer id) throws BusinessException {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if (null == outStockMapper) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND, "物资未找到");
        }
        if (outStock.getStatus() == 1) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_IS_REMOVE, "物资已经移入回收站了");
        }
        outStock.setStatus(1);
        outStockMapper.updateByPrimaryKey(outStock);
    }

    /**
     * 将指定出库记录还原
     * /business/inStock/back/111
     *
     * @param id
     * @return
     */
    @Override
    public void back(Integer id) throws BusinessException {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if (null == outStock) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND, "物资未找到");
        }
        if (outStock.getStatus() == 0) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_IS_REMOVE, "物资已经入库");
        }
        outStock.setStatus(0);
        outStockMapper.updateByPrimaryKey(outStock);
    }

    /**
     * 新增出库
     * @param outStockVO
     */
    @Override
    public void addOutStock(OutStockVO outStockVO, HttpServletRequest request) {
        System.out.println(outStockVO);
        //将InStockVO转为Instock
        OutStock outStock = new OutStock();
        BeanUtils.copyProperties(outStockVO, outStock);
        outStock.setOutNum(UUID.randomUUID().toString().substring(0,16)); //出库单编号
        outStock.setCreateTime(new Date());
        outStock.setStatus(2);
        String token = request.getHeader("Authorization");
        String username = JWTUtils.getUsername(token);
        outStock.setOperator(username);
//     todo   String token = null;
//        String username = JWTUtils.getUsername(token);//获取用户名
        //inStock.setOperator(username);
        List<Object> products = outStockVO.getProducts();//获取商品集合
        //将List<Object> 转换为List<Map>
        List<Map> maps = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            Map map = (Map) products.get(i);
            maps.add(map);
        }
        System.out.println(maps);
        int count = 0;
        for (Map map : maps) {
            Integer productId = (Integer) map.get("productId");
            Integer productNumber = (Integer) map.get("productNumber");
            //根据productId查询pNum
            Product product = productMapper.selectByPrimaryKey(productId);
            //根据pNum查询productstock
            ProductStock productStock = new ProductStock();
            productStock.setPNum(product.getPNum());
            //查询现有库存，并更新库存
            ProductStock productStock1 = productStockMapper.selectOne(productStock);
            productStock1.setStock(productStock1.getStock()-productNumber);
            productStockMapper.updateByPrimaryKey(productStock1);

            //保存OutStockInfo
            OutStockInfo outStockInfo = new OutStockInfo();
            outStockInfo.setOutNum(outStock.getOutNum());
            outStockInfo.setPNum(product.getPNum());
            outStockInfo.setProductNumber(productNumber);
            outStockInfo.setCreateTime(new Date());
            outStockInfo.setModifiedTime(new Date());
            outStockInfoMapper.insertSelective(outStockInfo);
            count+=productNumber;
        }
        outStock.setProductNumber(count);
        //如果SupplierId为空时 则需要添加消费人信息
        if(outStock.getConsumerId() == null){
            Consumer consumer = new Consumer();
            BeanUtils.copyProperties(outStockVO,consumer);
            consumer.setCreateTime(new Date());
            consumer.setModifiedTime(new Date());
            consumerMapper.insertSelective(consumer);
            outStock.setConsumerId(consumer.getId());
        }

        outStockMapper.insertSelective(outStock); //保存入库信息

        System.out.println(outStock.toString());
    }

    /**
     * business/outStock/publish/113
     * 修改出库状态
     * @param id
     * @return
     */
    @Override
    public void publish(Integer id) throws BusinessException {
        OutStock outStock = outStockMapper.selectByPrimaryKey(id);
        if(null == outStock){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"需要修改出库状态信息找不到");
        }
        outStock.setStatus(0);
        outStockMapper.updateByPrimaryKey(outStock);
    }
}
