package com.goods.business.service;

import com.goods.common.vo.business.HealthVO;
import com.goods.common.vo.system.PageVO;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-10 1:24
 * @version:
 */

public interface HealthService {
    HealthVO getHealth();

    void report(HealthVO healthVO, String username);

    PageVO<HealthVO> history(Integer pageNum, Integer pageSize);
}
