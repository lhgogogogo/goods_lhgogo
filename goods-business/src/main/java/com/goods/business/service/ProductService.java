package com.goods.business.service;

import com.goods.common.error.SystemException;
import com.goods.common.vo.business.ProductStockVO;
import com.goods.common.vo.business.ProductVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;
import java.util.Map;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 21:33
 * @version:
 */

public interface ProductService {
    PageVO<ProductVO> findProductList(Integer pageNum, Integer pageSize, String name, String categoryId,
                                      String supplier, String status,String categorys);

    void add(ProductVO productVO);

    ProductVO edit(Long id) throws SystemException;

    void update(Long id, ProductVO productVO) throws SystemException;

    void remove(Long id) throws SystemException;

    PageVO<ProductVO> findProducts(Map map);

    List<ProductStockVO> findProductStocks(Map map);

    //List<ProductStockVO> findAllStocks(Map map);
}
