package com.goods.business.service.imp;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.*;
import com.goods.business.service.InStockService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.error.SystemException;
import com.goods.common.model.business.*;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockItemVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;
import com.goods.system.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 1:58
 * @version:
 */

@Service
@SuppressWarnings("all")
public class InStockServiceImpl implements InStockService {

    @Autowired
    private InStockMapper inStockMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    @Autowired
    private InstockInfoMapper instockInfoMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductStockMapper productStockMapper;

    @Autowired
    private UserService userService;

    /**
     * 入库记录分页展示并查询
     *
     * @param map
     * @return pageNum=1&pageSize=10&status=0&type=1&inNum=ffff&startTime=2021-11-25+00:00:00&endTime=2021-12-22+00:00:00
     */
    @Override
    public PageVO<InStockVO> findInStockList(Map map) {
        String pageNum = (String) map.get("pageNum");  //页码数
        String pageSize = (String) map.get("pageSize");//行数
        String status = (String) map.get("status"); //状态
        String type = (String) map.get("type"); //物资类型
        String inNum = (String) map.get("inNum"); //物资订单号
        String startTime = (String) map.get("startTime");//开始时间
        String endTime = (String) map.get("endTime");//结束时间
        //创建example和criteria对象进行指定查询
        Example example = new Example(InStock.class);
        Example.Criteria criteria = example.createCriteria();
        //匹配订单状态
        if (!StringUtils.isEmpty(status)) {
            criteria.andEqualTo("status", Integer.parseInt(status));
        }
        //物资类型
        if (!StringUtils.isEmpty(type)) {
            criteria.andEqualTo("type", Integer.parseInt(type));
        }
        //物资订单号
        if (!StringUtils.isEmpty(inNum)) {
            criteria.andEqualTo("inNum", inNum);
        }
        //匹配时间
        if (!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime)) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                long start = simpleDateFormat.parse(startTime).getTime();
//                long end = simpleDateFormat.parse(endTime).getTime();
                Date start = simpleDateFormat.parse(startTime);
                Date end = simpleDateFormat.parse(endTime);
                criteria.andGreaterThanOrEqualTo("modified", end);
                criteria.andLessThanOrEqualTo("createTime", start);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //分页
        List<InStock> inStocks = inStockMapper.selectByExample(example);
        PageHelper.startPage(Integer.parseInt(pageNum), Integer.parseInt(pageSize));
        List<InStockVO> inStockVOS = new ArrayList<>();
        inStocks.forEach(inStock -> {
            Supplier supplier = supplierMapper.selectByPrimaryKey(inStock.getSupplierId());
            InStockVO inStockVO = new InStockVO();
            BeanUtils.copyProperties(inStock, inStockVO);
            inStockVO.setSupplierName(supplier.getName());
            inStockVO.setPhone(supplier.getPhone());
            inStockVOS.add(inStockVO);
        });
        PageInfo<InStock> pageInfo = new PageInfo<>(inStocks);
        return new PageVO<>(pageInfo.getTotal(), inStockVOS);
    }

    /**
     * 查看入库单明细
     *
     * @param id
     * @param pageNum
     * @return
     */
    @Override
    public InStockDetailVO detail(Integer id, Integer pageNum) throws BusinessException {
        //根据id查询InStock对象
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        //如果查询为空
        if (null == inStock) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND, "物资未找到");
        }
        InStockDetailVO inStockDetailVO = new InStockDetailVO();
        inStockDetailVO.setInNum(inStock.getInNum());
        inStockDetailVO.setStatus(inStock.getStatus());
        inStockDetailVO.setType(inStock.getType());
        inStockDetailVO.setOperator(inStock.getOperator());
        //查询供应商信息
        Supplier supplier = supplierMapper.selectByPrimaryKey(inStock.getSupplierId());
        if (null != supplier) {
            SupplierVO supplierVO = new SupplierVO();
            BeanUtils.copyProperties(supplier, supplierVO);
            inStockDetailVO.setSupplierVO(supplierVO);
        }
        //根据InStock的InNum查询InStockInfo
        Example exampleInStockInfo = new Example(InStockInfo.class);
        Example.Criteria criteriaInStockInfo = exampleInStockInfo.createCriteria();
        criteriaInStockInfo.andEqualTo("inNum", inStock.getInNum());
        //查询出InStockInfo集合
        List<InStockInfo> inStockInfos = instockInfoMapper.selectByExample(exampleInStockInfo);
        //获取商品编号集合
        List<String> pNums = inStockInfos.stream().map(InStockInfo::getPNum).collect(Collectors.toList());
        //用于封装最后的InStockItemVO集合
        List<InStockItemVO> inStockItemVOS = new ArrayList<>();

        for (String pNum : pNums) {
            //查询商品信息
            Example exampleProduct = new Example(Product.class);
            Example.Criteria criteriaProduct = exampleProduct.createCriteria();
            criteriaProduct.andEqualTo("pNum", pNum);
            Product product = productMapper.selectOneByExample(exampleProduct);
            InStockItemVO inStockItemVO = new InStockItemVO();
            BeanUtils.copyProperties(product, inStockItemVO);
            //查询商品库存
            Example exampleProductStock = new Example(ProductStock.class);
            Example.Criteria criteriaProductStock = exampleProductStock.createCriteria();
            criteriaProductStock.andEqualTo("pNum", pNum);
            ProductStock productStock = productStockMapper.selectOneByExample(exampleProductStock);
            inStockItemVO.setCount(Math.toIntExact(productStock.getStock()));
            inStockItemVOS.add(inStockItemVO);
        }
        inStockDetailVO.setItemVOS(inStockItemVOS);

        return inStockDetailVO;
    }

    /**
     * 将指定入库记录移至回收站
     * //business/inStock/remove/112
     *
     * @param id
     * @return
     */
    @Override
    public void remove(Integer id) throws BusinessException {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if (null == inStock) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND, "物资未找到");
        }
        if (inStock.getStatus() == 1) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_IS_REMOVE, "物资已经移入回收站了");
        }
        inStock.setStatus(1);
        inStock.setModified(new Date());
        inStockMapper.updateByPrimaryKey(inStock);
    }

    /**
     * 将指定入库记录还原
     * /business/inStock/back/111
     *
     * @param id
     * @return
     */
    @Override
    public void back(Integer id) throws BusinessException {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if (null == inStock) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND, "物资未找到");
        }
        if (inStock.getStatus() == 0) {
            throw new BusinessException(BusinessCodeEnum.PRODUCT_IS_REMOVE, "物资已经入库");
        }
        inStock.setStatus(0);
        inStock.setModified(new Date());
        inStockMapper.updateByPrimaryKey(inStock);
    }

    /**
     * 新增入库
     * /business/inStock/addIntoStock
     *
     * @param map
     * @return
     */
    @Override
    public void addIntoStock(InStockVO inStockVO, HttpServletRequest request) {
        System.out.println(inStockVO);
        //将InStockVO转为Instock
        InStock inStock = new InStock();
        BeanUtils.copyProperties(inStockVO, inStock);
        inStock.setInNum(UUID.randomUUID().toString()); //入库单编号
        inStock.setCreateTime(new Date());
        inStock.setModified(new Date());
        inStock.setStatus(2);
        try {
            String username = userService.info().getUsername();
            inStock.setOperator(username);
        } catch (SystemException e) {
            e.printStackTrace();
        }
//        String token = null;
//        String username = JWTUtils.getUsername(token);//获取用户名
        //inStock.setOperator(username);
        List<Object> products = inStockVO.getProducts();//获取商品集合
        //将List<Object> 转换为List<Map>
        List<Map> maps = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            Map map = (Map) products.get(i);
            maps.add(map);
        }
        System.out.println(maps);
        int count = 0;
        for (Map map : maps) {
            Integer productId = (Integer) map.get("productId");
            Integer productNumber = (Integer) map.get("productNumber");
            //根据productId查询pNum
            Product product = productMapper.selectByPrimaryKey(productId);
            //根据pNum查询productstock
            ProductStock productStock = new ProductStock();
            productStock.setPNum(product.getPNum());
            //查询现有库存，并更新库存
            ProductStock productStock1 = productStockMapper.selectOne(productStock);
            productStock1.setStock(productStock1.getStock()+productNumber);
            productStockMapper.updateByPrimaryKey(productStock1);

            //保存OutStockInfo
            InStockInfo inStockInfo = new InStockInfo();
            inStockInfo.setInNum(inStock.getInNum());
            inStockInfo.setPNum(product.getPNum());
            inStockInfo.setProductNumber(productNumber);
            inStockInfo.setCreateTime(new Date());
            inStockInfo.setModifiedTime(new Date());
            instockInfoMapper.insertSelective(inStockInfo);
            count+=productNumber;
        }
        inStock.setProductNumber(count);
        //如果SupplierId为空时 则需要添加捐赠人信息
        if(inStock.getSupplierId() == null){
            Supplier supplier = new Supplier();
            BeanUtils.copyProperties(inStockVO,supplier);
            supplier.setCreateTime(new Date());
            supplier.setModifiedTime(new Date());
            supplierMapper.insertSelective(supplier);
            inStock.setSupplierId(supplier.getId());
        }


        inStockMapper.insertSelective(inStock); //保存入库信息

        System.out.println(inStock.toString());
    }

    /**
     * business/inStock/publish/113
     * 修改入库状态
     * @param id
     * @return
     */
    @Override
    public void publish(Integer id) throws BusinessException {
        InStock inStock = inStockMapper.selectByPrimaryKey(id);
        if(null == inStock){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"需要修改物资找不到");
        }
        inStock.setStatus(0);
        inStockMapper.updateByPrimaryKey(inStock);
    }
}

