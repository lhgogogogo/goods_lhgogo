package com.goods.business.service;

import com.goods.common.error.BusinessException;
import com.goods.common.vo.business.InStockDetailVO;
import com.goods.common.vo.business.InStockVO;
import com.goods.common.vo.system.PageVO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 1:58
 * @version:
 */

public interface InStockService {
    PageVO<InStockVO> findInStockList(Map map);

    InStockDetailVO detail(Integer id, Integer pageNum) throws BusinessException;

    void remove(Integer id) throws BusinessException;

    void back(Integer id) throws BusinessException;

    void addIntoStock(InStockVO map, HttpServletRequest request);

    void publish(Integer id) throws BusinessException;
}
