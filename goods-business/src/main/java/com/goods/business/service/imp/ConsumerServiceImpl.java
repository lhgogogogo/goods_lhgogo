package com.goods.business.service.imp;

import com.github.pagehelper.PageInfo;
import com.goods.business.mapper.ConsumerMapper;
import com.goods.business.service.ConsumerService;
import com.goods.common.error.BusinessCodeEnum;
import com.goods.common.error.BusinessException;
import com.goods.common.model.business.Consumer;
import com.goods.common.vo.business.ConsumerVO;
import com.goods.common.vo.system.PageVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-09 20:50
 * @version:
 */

@Service
public class ConsumerServiceImpl implements ConsumerService {
    @Autowired
    private ConsumerMapper consumerMapper;

    /**
     * 物资去处分页并条件查询
     * pageNum=1&pageSize=10&name=aaa&address=%E5%AE%89%E5%BE%BD&contact=%E6%9E%97%E8%BE%89
     * @param map
     * @return
     */
    @Override
    public PageVO<ConsumerVO> findConsumerList(Map map) {
        String pageNum = (String) map.get("pageNum");  //页码数
        String pageSize = (String) map.get("pageSize");//行数
        String name = (String) map.get("name");//物资具体发放
        String address = (String) map.get("name");//地址
        String contact = (String) map.get("contact");//姓名
        List<Consumer> consumerList =  consumerMapper.selectConsumer(map);
        List<ConsumerVO> consumerVOList = consumerList.stream().map(consumer -> {
            ConsumerVO consumerVO = new ConsumerVO();
            BeanUtils.copyProperties(consumer, consumerVO);
            return consumerVO;
        }).collect(Collectors.toList());
        PageInfo<Consumer> pageInfo = new PageInfo<>(consumerList);
        return new PageVO<>(pageInfo.getTotal(), consumerVOList);
        /*Example example = new Example(Consumer.class);
        Example.Criteria criteria = example.createCriteria();
        if(!StringUtils.isEmpty(address)){
            criteria.
            criteria.andEqualTo("address",name);
        }
        if(!StringUtils.isEmpty(name)){
            criteria.andEqualTo("name",name);
        }
        if(!StringUtils.isEmpty(contact)){
            criteria.andEqualTo("contact",contact);
        }*/

    }

    /**
     * 添加物资去处
     * @param consumerVO
     */
    @Override
    public void add(ConsumerVO consumerVO) {
        Consumer consumer = new Consumer();
        BeanUtils.copyProperties(consumerVO,consumer);
        consumer.setCreateTime(new Date());
        consumer.setModifiedTime(new Date());
        consumerMapper.insertSelective(consumer);
    }

    /**
     * 编辑物资去处
     * @param id
     * @return
     */
    @Override
    public ConsumerVO edit(Long id) throws BusinessException {
        Consumer consumer = consumerMapper.selectByPrimaryKey(id);
        if(null == consumer){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"物资未找到");
        }
        ConsumerVO consumerVO = new ConsumerVO();
        BeanUtils.copyProperties(consumer,consumerVO);
        return consumerVO;
    }

    /**
     * 更新物资去处
     * @param id
     * @param consumerVO
     * @throws BusinessException
     */
    @Override
    public void update(Long id, ConsumerVO consumerVO) throws BusinessException {
        Consumer consumer = consumerMapper.selectByPrimaryKey(id);
        if(null == consumer){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"需要更新的物资未找到");
        }
        Consumer consumer1 = new Consumer();
        BeanUtils.copyProperties(consumerVO,consumer1);
        consumer1.setModifiedTime(new Date());
        consumerMapper.updateByPrimaryKey(consumer1);
    }

    /**
     * 删除指定物资去处
     * @param id
     */
    @Override
    public void delete(Long id) throws BusinessException {
        Consumer consumer = consumerMapper.selectByPrimaryKey(id);
        if(null == consumer){
            throw new BusinessException(BusinessCodeEnum.PRODUCT_NOT_FOUND,"需要删除的物资未找到");
        }
        consumerMapper.deleteByPrimaryKey(id);
    }

    /**
     * 查找所有物资去向
     *  //business/consumer/findAll
     * @param
     * @return
     */
    @Override
    public List<ConsumerVO> findAll() {
        List<Consumer> consumers = consumerMapper.selectAll();
        List<ConsumerVO> consumerVOList = consumers.stream().map(consumer -> {
            ConsumerVO consumerVO = new ConsumerVO();
            BeanUtils.copyProperties(consumer, consumerVO);
            return consumerVO;
        }).collect(Collectors.toList());
        return consumerVOList;
    }
}
