package com.goods.business.service;

import com.goods.common.error.BusinessException;
import com.goods.common.error.SystemException;
import com.goods.common.vo.business.SupplierVO;
import com.goods.common.vo.system.PageVO;

import java.util.List;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 19:15
 * @version:
 */

public interface SupplierService {
    PageVO<SupplierVO> findSupplierList(Integer pageNum, Integer pageSize, SupplierVO supplierVO);

    void add(SupplierVO supplierVO);

    SupplierVO edit(Long id) throws BusinessException;

    void update(Long id, SupplierVO supplierVO) throws SystemException;

    void delete(Long id) throws SystemException;

    List<SupplierVO> findAll();
}
