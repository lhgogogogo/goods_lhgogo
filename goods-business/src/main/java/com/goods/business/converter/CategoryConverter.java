package com.goods.business.converter;

import com.goods.common.model.business.ProductCategory;
import com.goods.common.vo.business.ProductCategoryTreeNodeVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ：linhui
 * @description ：
 * @date ：2021-11-08 12:10
 * @version:
 */
@SuppressWarnings("all")
public class CategoryConverter {

    /**
     * 将ProductCategory转换为ProductCategoryVO
     * @param productCategories
     * @return
     */
    public static List<ProductCategoryTreeNodeVO> converterToProductCategoryVO(List<ProductCategory> productCategories){
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOListOne = new ArrayList<>();
        for (ProductCategory productCategory : productCategories) {
            //保存一级目录
            ProductCategoryTreeNodeVO productCategoryTreeNodeVO1 = new ProductCategoryTreeNodeVO();
            if(0 == productCategory.getPid()){
                BeanUtils.copyProperties(productCategory,productCategoryTreeNodeVO1);
                productCategoryTreeNodeVO1.setLev(1);
                productCategoryTreeNodeVOListOne.add(productCategoryTreeNodeVO1);
            }
        }
        for (ProductCategoryTreeNodeVO productCategoryTreeNodeVO : productCategoryTreeNodeVOListOne) {
            List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOListTwo = new ArrayList<>();
            for (ProductCategory productCategory : productCategories) {
                ProductCategoryTreeNodeVO productCategoryTreeNodeVO2 = new ProductCategoryTreeNodeVO();
                if (productCategoryTreeNodeVO.getId() == productCategory.getPid()) {
                    BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO2);
                    productCategoryTreeNodeVO2.setLev(2);
                    productCategoryTreeNodeVOListTwo.add(productCategoryTreeNodeVO2);
                }
            }
            productCategoryTreeNodeVO.setChildren(productCategoryTreeNodeVOListTwo);
        }
        return productCategoryTreeNodeVOListOne;
    }

    /**
     * 将ProductCategory集合转换为ProductCategoryTreeNodeVO集合
     * @param productCategories
     * @return
     */
    public static List<ProductCategoryTreeNodeVO> converterToProductCategoryTreeNodeVO(List<ProductCategory> productCategories){
        List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOListOne = new ArrayList<>();
        for (ProductCategory productCategory : productCategories) {
            //保存一级目录
            ProductCategoryTreeNodeVO productCategoryTreeNodeVO1 = new ProductCategoryTreeNodeVO();
            if(0 == productCategory.getPid()){
                BeanUtils.copyProperties(productCategory,productCategoryTreeNodeVO1);
                productCategoryTreeNodeVO1.setLev(1);
                productCategoryTreeNodeVOListOne.add(productCategoryTreeNodeVO1);
            }
        }
        //二级目录

        for (ProductCategoryTreeNodeVO productCategoryTreeNodeVO : productCategoryTreeNodeVOListOne) {
            List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOListTwo = new ArrayList<>();
            for (ProductCategory productCategory : productCategories) {
            ProductCategoryTreeNodeVO productCategoryTreeNodeVO2 = new ProductCategoryTreeNodeVO();
                if (productCategoryTreeNodeVO.getId() == productCategory.getPid()) {
                    BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO2);
                    productCategoryTreeNodeVO2.setLev(2);
                    productCategoryTreeNodeVOListTwo.add(productCategoryTreeNodeVO2);
                }
            }
            productCategoryTreeNodeVO.setChildren(productCategoryTreeNodeVOListTwo);

            for (ProductCategoryTreeNodeVO productCategoryTreeNodeVO1 : productCategoryTreeNodeVOListTwo) {
                List<ProductCategoryTreeNodeVO> productCategoryTreeNodeVOListThree = new ArrayList<>();
                for (ProductCategory productCategory : productCategories) {
                    ProductCategoryTreeNodeVO productCategoryTreeNodeVO2 = new ProductCategoryTreeNodeVO();
                    if (productCategoryTreeNodeVO1.getId() == productCategory.getPid()) {
                        BeanUtils.copyProperties(productCategory, productCategoryTreeNodeVO2);
                        productCategoryTreeNodeVO2.setLev(3);
                        productCategoryTreeNodeVOListThree.add(productCategoryTreeNodeVO2);
                    }
                }
                productCategoryTreeNodeVO1.setChildren(productCategoryTreeNodeVOListThree);
            }
        }
        Collections.sort(productCategoryTreeNodeVOListOne,ProductCategoryTreeNodeVO.order() );//排序
        return productCategoryTreeNodeVOListOne;
    }
}
